<?php

    require_once './animal.php';
    require_once './ape.php';
    require_once './frog.php';

    $sheep = new Animal("shaun");
    echo "Name : " . $sheep -> name . "<br>";
    echo "Legs : " . $sheep -> legs . "<br>";
    echo "Cold Blooded : "; var_dump ($sheep -> cold_bloodeed);
    echo "<br><br>";

    $sungokong = new Ape("kera sakti");
    echo "Name : " . $sungokong -> name . "<br>";
    echo "Legs : " . $sungokong -> legs . "<br>";
    echo "Cold Blooded : "; var_dump ($sungokong -> cold_bloodeed);
    echo "<br> Yell : ";
    $sungokong->yell(); // "Auooo"
    echo "<br><br>";

    $kodok = new Frog("buduk");
    echo "Name : " . $kodok -> name . "<br>";
    echo "Legs : " . $kodok -> legs . "<br>";
    echo "Cold Blooded : "; var_dump ($kodok -> cold_bloodeed);
    echo "<br> Jump : ";
    $kodok->jump(); // "hop hop"
    echo "<br>";
?>